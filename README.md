Snap IPC
========

This repo provides a client *library* for the snap-stack autopilot (linked to `snap`) and a *binary* server (ran as a `systemd` service on VOXL). This client/server inter-process communication allows the dockerized ROS snap-stack to communicate with the `ioboard` via `SnapIO` (which runs on the VOXL hardware and cannot run in the Ubuntu docker container).


## Build Instructions

1. Follow the instructions [here](https://gitlab.com/voxl-public/voxl-docker) to build and install the `voxl-emulator` docker image
1. Pull in the [`snapio-protocol`](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/snapio-protocol) git submodule

    ```bash
    git submodule update --init --recursive
    ```
1. Launch the `voxl-emulator` docker and make sure this project directory is mounted inside the Docker.

    ````bash
    ~/snap_ipc $ voxl-docker -i voxl-emulator
    voxl-emulator:~$
    ```
1. Install dependencies inside the docker. Specify the dependencies should be pulled from either the development (dev) or stable modalai package repos. If you are unsure, specify `stable`.

    ```bash
    voxl-emulator:~$ ./install_build_deps.sh stable
    ```
1. Build the package

    ```bash
    voxl-emulator:~$ ./build.sh native
    ```

## Installation

1. Generate an ipk package inside the docker.

    ```bash
    voxl-emulator:~$ ./make_package.sh
    ````
2. You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

    ```bash
    ~/snap_ipc$ ./install_on_voxl.sh
    ```
## FAQ

- How to manually start / stop server?
    - Start: `systemctl start snap-ipc-server`
    - Stop: `systemctl stop snap-ipc-server`
- How to check if server is running / which port is opened?
    - `systemctl status snap-ipc-server` should say "Running". You may be able to see a line saying "Connecting to ioboard via UART on J7 @ 921600 baud". If not, use `journalctl -u snap-ipc-server` to see the full log (add `-f` to follow the log)
- How to change port / baudrate of connection?
    - Edit the `/etc/snap/snap-ipc-server.conf` file. Recommended baud is 921600. Available ports are J7, J10, J11, J12.
    - After editing, make sure to stop/start (or use `restart`) to restart the server. Check that port opened (using `status`).
    - For sfpro, see [here](https://developer.qualcomm.com/hardware/qualcomm-flight-pro/board-pin-outs)
    - For VOXL with built-in Flight Core, you shouldn't need to change anything