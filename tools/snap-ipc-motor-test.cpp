/**
 * @file snap-ipc-motor-test.cpp
 * @brief CLI tool to send motor commands via snap ipc and mpa
 * @author Parker Lusk <plusk@mit.edu>
 * @date 13 July 2021
 */

#include <array>
#include <chrono>
#include <iostream>
#include <thread>

#include <modal_pipe.h>
#include <snap_ipc/client.h>

int main(int argc, char const *argv[])
{
  acl::snapipc::Client client;

  static constexpr float thr_val = 0.0f;
  std::array<float, 6> throttle;
  throttle.fill(thr_val);

  std::cout << "Setting motor throttles to " << thr_val << std::endl;

  // spin forever with sleep to reduce busy waiting
  while(true) {
    client.set_motors(throttle);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }

  return 0;
}
