/**
 * @file server.h
 * @brief SnapIO server for mpa connections
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 June 2021
 */

#pragma once

#include <array>
#include <cstdint>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

#include <modal_pipe.h>

#include <snap_io/snap_io.h>

namespace acl {
namespace snapipc {

  class Server
  {
  public:
    Server();
    ~Server();

  private:
    std::unique_ptr<acl::snapio::SnapIO> snapio_; ///< API for ioboard access

    // \brief Default ioboard UART connection params
    acl::snapio::UART uart_ = acl::snapio::UART::J12;
    int baud_ = 921600;

    void create_pipes();
    void close_pipes();
    bool config_file_read();

  // \brief The following are public due to c-style callbacks
  public:
    void rx_cb(const uint8_t* data, int bytes);
  };

} // ns snapipc
} // ns acl
