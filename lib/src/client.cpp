/**
 * @file client.cpp
 * @brief API for accessing snapipc ioboard via mpa
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 June 2021
 */

#include "snap_ipc/client.h"

#include "snapio-protocol/sio_serial.h"

static constexpr int RX_CH = 0; ///< arbitrary, unique channel number
static constexpr int TX_CH = 1; ///< arbitrary, unique channel number

// ----------------------------------------------------------------------------
// C-style Pipe Callbacks - RX_CH
// ----------------------------------------------------------------------------

// called whenever we connect or reconnect to the server
static void _rx_connect_cb(int ch, int client_id, char* client_name, void* context)
{
  acl::snapipc::Client* client = reinterpret_cast<acl::snapipc::Client*>(context);
  std::cout << "RX_CH connected to " << client_name << std::endl;
}

// called whenever we disconnect from the server
static void _rx_disconnect_cb(int ch, int client_id, char* client_name, void* context)
{
  acl::snapipc::Client* client = reinterpret_cast<acl::snapipc::Client*>(context);
  std::cout << "RX_CH connected to " << client_name << std::endl;
}

// called when control commands are received
static void _rx_control_pipe_cb(int ch, char* string, int bytes, void* context)
{
  // acl::snapipc::Client* server = reinterpret_cast<acl::snapipc::Client*>(context);
}

// ----------------------------------------------------------------------------
// C-style Pipe Callbacks - TX_CH
// ----------------------------------------------------------------------------

// called whenever we connect or reconnect to the server
static void _tx_connect_cb(int ch, void* context)
{
  acl::snapipc::Client* client = reinterpret_cast<acl::snapipc::Client*>(context);
  std::cout << "TX_CH connected" << std::endl;
}


// called whenever we disconnect from the server
static void _tx_disconnect_cb(int ch, void* context)
{
  acl::snapipc::Client* client = reinterpret_cast<acl::snapipc::Client*>(context);
  std::cout << "TX_CH disconnected" << std::endl;
}

// called when the simple helper has data for us
static void _tx_helper_cb(int ch, char* data, int bytes, void* context)
{
  acl::snapipc::Client* client = reinterpret_cast<acl::snapipc::Client*>(context);

  if (ch == TX_CH) {
    client->tx_cb(reinterpret_cast<uint8_t*>(data), bytes);
  }
}

// ============================================================================

namespace acl {
namespace snapipc {

Client::Client()
{
  create_pipes();
}

// ----------------------------------------------------------------------------

Client::~Client()
{
  close_pipes();
}

// ----------------------------------------------------------------------------

void Client::set_motors(const std::array<float, 6>& throttle)
{
  sio_serial_motorcmd_msg_t msg;

  // Convert motor throttle to PWM
  for (size_t i=0; i<throttle.size(); ++i) {
    if (throttle[i] >= 0 && throttle[i] <= 1.) {
      msg.throttle[i] = throttle[i];
    } else if (throttle[i] > 1) {
      msg.throttle[i] = 1.0f;
    } else {
      msg.throttle[i] = 0.0f;
    }
  }

  uint8_t buf[SIO_SERIAL_MAX_MESSAGE_LEN];
  const int len = sio_serial_motorcmd_msg_send_to_buffer(buf, &msg);

  pipe_server_write(RX_CH, reinterpret_cast<const char*>(buf), len);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void Client::create_pipes()
{
  // Pipe constants
  static constexpr const char * CLIENT_NAME = "snap-ipc-client-tx";
  // static constexpr const char * SERVER_NAME = "snap-ipc-client-rx";
  // static constexpr const char * RX_PIPE_PATH = "snapipc/rx";
  static constexpr const char * TX_PIPE_PATH = "snapipc/tx";

  //
  // Rx pipe (pipe server, incoming to ioboard)
  //

  pipe_info_t info = {
    "rx",                                     // name
    MODAL_PIPE_DEFAULT_BASE_DIR "snapipc/rx", // location
    "sio_serial_message_t",                   // type
    "snap-ipc-client-rx",                     // server_name
    MODAL_PIPE_DEFAULT_PIPE_SIZE              // size_bytes
  };

  int flags = 0; //SERVER_FLAG_EN_CONTROL_PIPE;
  // pipe_server_set_control_cb(RX_CH, _rx_control_pipe_cb, this);
  pipe_server_set_connect_cb(RX_CH, _rx_connect_cb, this);
  pipe_server_set_disconnect_cb(RX_CH, _rx_disconnect_cb, this);
  if (pipe_server_create(RX_CH, info, flags)) {
    throw std::runtime_error("Error creating pipe server for RX_CH");
  }
  // pipe_server_set_available_control_commands(RX_CH, CONTROL_COMMANDS);

  //
  // Tx pipe (pipe client, outgoing from ioboard)
  //

  pipe_client_set_simple_helper_cb(TX_CH, _tx_helper_cb, this);
  pipe_client_set_connect_cb(TX_CH, _tx_connect_cb, this);
  pipe_client_set_disconnect_cb(TX_CH, _tx_disconnect_cb, this);

  int ret = pipe_client_open(TX_CH, TX_PIPE_PATH, CLIENT_NAME,
                  EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT,
                  MODAL_PIPE_DEFAULT_PIPE_SIZE);

  if (ret < 0) {
    pipe_print_error(ret);
    throw std::runtime_error("Error creating pipe client for TX_CH");
  }
}

// ----------------------------------------------------------------------------

void Client::close_pipes()
{
  pipe_server_close(RX_CH);
  pipe_client_close(TX_CH);
}

// ----------------------------------------------------------------------------
// Callbacks
// ----------------------------------------------------------------------------

void Client::tx_cb(const uint8_t* data, int bytes)
{

  if (bytes > SIO_SERIAL_MAX_MESSAGE_LEN) {
    throw std::runtime_error("Received message from TX_CH has more bytes than expected.");
  }

  const sio_serial_message_t &msg = reinterpret_cast<const sio_serial_message_t&>(*data);

  switch (msg.type) {
    case SIO_SERIAL_MSG_MOTORCMD:
    {
      sio_serial_motorcmd_msg_t payload;
      sio_serial_motorcmd_msg_unpack(&payload, &msg);
      // motor_cb(payload);
      break;
    }
  }
}

} // ns snapipc
} // ns acl