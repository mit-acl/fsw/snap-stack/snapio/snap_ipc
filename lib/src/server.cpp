/**
 * @file server.cpp
 * @brief SnapIO server for mpa connections
 * @author Parker Lusk <plusk@mit.edu>
 * @date 28 June 2021
 */

#include "snap_ipc/server.h"

#include "snapio-protocol/sio_serial.h"

static constexpr int RX_CH = 0; ///< arbitrary, unique channel number
static constexpr int TX_CH = 1; ///< arbitrary, unique channel number

// ----------------------------------------------------------------------------
// C-style Pipe Callbacks - RX_CH
// ----------------------------------------------------------------------------

// called whenever we connect or reconnect to the server
static void _rx_connect_cb(int ch, void* context)
{
  acl::snapipc::Server* server = reinterpret_cast<acl::snapipc::Server*>(context);
  // server->set_connected();

  std::cout << "RX_CH connected" << std::endl;
}

// called whenever we disconnect from the server
static void _rx_disconnect_cb(int ch, void* context)
{
  acl::snapipc::Server* server = reinterpret_cast<acl::snapipc::Server*>(context);
  // server->set_disconnected();

  std::cout << "RX_CH disconnected" << std::endl;
}

// called when the simple helper has data for us
static void _rx_helper_cb(int ch, char* data, int bytes, void* context)
{
  acl::snapipc::Server* server = reinterpret_cast<acl::snapipc::Server*>(context);

  if (ch == RX_CH) {
    server->rx_cb(reinterpret_cast<uint8_t*>(data), bytes);
  }
}

// ----------------------------------------------------------------------------
// C-style Pipe Callbacks - TX_CH
// ----------------------------------------------------------------------------

// called whenever we connect or reconnect to the server
static void _tx_connect_cb(int ch, int client_id, char* client_name, void* context)
{
  acl::snapipc::Server* server = reinterpret_cast<acl::snapipc::Server*>(context);
  std::cout << "TX_CH connected to " << client_name << std::endl;
}

// called whenever we disconnect from the server
static void _tx_disconnect_cb(int ch, int client_id, char* client_name, void* context)
{
  acl::snapipc::Server* server = reinterpret_cast<acl::snapipc::Server*>(context);
  std::cout << "TX_CH connected to " << client_name << std::endl;
}

// called when control commands are received
static void _tx_control_pipe_cb(int ch, char* string, int bytes, void* context)
{
  // acl::snapipc::Server* server = reinterpret_cast<acl::snapipc::Server*>(context);
}

// ============================================================================

namespace acl {
namespace snapipc {

std::string get_uart_str(const acl::snapio::UART& uart)
{
  if (uart == acl::snapio::UART::J7)  return "J7";
  if (uart == acl::snapio::UART::J10) return "J10";
  if (uart == acl::snapio::UART::J11) return "J11";
  if (uart == acl::snapio::UART::J12) return "J12";
  return "J??";
}

// ----------------------------------------------------------------------------

acl::snapio::UART get_uart(const std::string& str, const acl::snapio::UART& def)
{
  if (str.compare("j7") == 0 || str.compare("J7") == 0) return acl::snapio::UART::J7;
  if (str.compare("j10") == 0 || str.compare("J10") == 0) return acl::snapio::UART::J10;
  if (str.compare("j11") == 0 || str.compare("J11") == 0) return acl::snapio::UART::J11;
  if (str.compare("j12") == 0 || str.compare("J12") == 0) return acl::snapio::UART::J12;
  return def;
}

// ----------------------------------------------------------------------------

Server::Server()
{
  if (!config_file_read()) {
    throw std::runtime_error("Error loading config file");
  }

  // connect to ioboard via SnapIO
  std::cout << "Connecting to ioboard via UART on " << get_uart_str(uart_);
  std::cout << " @ " << baud_ << " baud" << std::endl;
  snapio_.reset(new acl::snapio::SnapIO(uart_, baud_));

  create_pipes();
}

// ----------------------------------------------------------------------------

Server::~Server()
{
  close_pipes();
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void Server::create_pipes()
{
  // Pipe constants
  static constexpr const char * CLIENT_NAME = "snap-ipc-server-rx";
  // static constexpr const char * SERVER_NAME = "snap-ipc-server-tx";
  static constexpr const char * RX_PIPE_PATH = "snapipc/rx";
  // static constexpr const char * TX_PIPE_PATH = "snapipc/tx";

  //
  // Rx pipe (pipe client, incoming to ioboard)
  //

  pipe_client_set_simple_helper_cb(RX_CH, _rx_helper_cb, this);
  pipe_client_set_connect_cb(RX_CH, _rx_connect_cb, this);
  pipe_client_set_disconnect_cb(RX_CH, _rx_disconnect_cb, this);

  int ret = pipe_client_open(RX_CH, RX_PIPE_PATH, CLIENT_NAME,
                  EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT,
                  MODAL_PIPE_DEFAULT_PIPE_SIZE);

  if (ret < 0) {
    pipe_print_error(ret);
    throw std::runtime_error("Error creating pipe client for RX_CH");
  }

  //
  // Tx pipe (pipe server, outgoing from ioboard)
  //

  pipe_info_t info = {
    "tx",                                     // name
    MODAL_PIPE_DEFAULT_BASE_DIR "snapipc/tx", // location
    "sio_serial_message_t",                   // type
    "snap-ipc-server-tx",                     // server_name
    MODAL_PIPE_DEFAULT_PIPE_SIZE              // size_bytes
  };

  int flags = 0; //SERVER_FLAG_EN_CONTROL_PIPE;
  // pipe_server_set_control_cb(TX_CH, _tx_control_pipe_cb, this);
  pipe_server_set_connect_cb(TX_CH, _tx_connect_cb, this);
  pipe_server_set_disconnect_cb(TX_CH, _tx_disconnect_cb, this);
  if (pipe_server_create(TX_CH, info, flags)) {
    throw std::runtime_error("Error creating pipe server for TX_CH");
  }
  // pipe_server_set_available_control_commands(TX_CH, CONTROL_COMMANDS);
}

// ----------------------------------------------------------------------------

void Server::close_pipes()
{
  pipe_client_close(RX_CH);
  pipe_server_close(TX_CH);
}

// ----------------------------------------------------------------------------

bool Server::config_file_read()
{
  static constexpr char const * SNAP_IPC_SERVER_CONF_FILE = "/etc/snap/snap-ipc-server.conf";

  int ret = json_make_empty_file_if_missing(SNAP_IPC_SERVER_CONF_FILE);
  if (ret < 0) return false;
  else if (ret > 0) std::cerr << "Creating new config file: " << SNAP_IPC_SERVER_CONF_FILE << std::endl;

  cJSON* parent = json_read_file(SNAP_IPC_SERVER_CONF_FILE);
  if (parent == nullptr) return false;

  static constexpr int MAXLEN = 50;
  char uartstr[MAXLEN];

  json_fetch_string_with_default(parent, "uart", uartstr, MAXLEN, get_uart_str(uart_).c_str());
  json_fetch_int_with_default(parent, "baud", &baud_, baud_);

  uart_ = get_uart(std::string{uartstr}, uart_);

  if (json_get_parse_error_flag()) {
    std::cerr << "failed to parse config file" << std::endl;
    cJSON_Delete(parent);
    return false;
  }

  // write modified data to disk if neccessary
  if (json_get_modified_flag()) {
    json_write_to_file(SNAP_IPC_SERVER_CONF_FILE, parent);
  }
  cJSON_Delete(parent);

  return true;
}

// ----------------------------------------------------------------------------
// Callbacks
// ----------------------------------------------------------------------------

void Server::rx_cb(const uint8_t* data, int bytes)
{
  // TODO: this is often true - should probably profile latencies, dropped packet, etc?
  // if (bytes > SIO_SERIAL_MAX_MESSAGE_LEN) {
  //   std::cerr << "Received message from RX_CH has more bytes than expected." << std::endl;
  // }

  snapio_->forward_buffer(data, bytes);
}

} // ns snapipc
} // ns acl